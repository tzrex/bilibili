FROM node:lts-alpine

WORKDIR /app

ENV TZ="Asia/Shanghai"

RUN npm config set registry https://repo.huaweicloud.com/repository/npm/
# RUN npm cache clean -f

# 安装依赖
COPY package.json ./package.json
COPY package-lock.json ./package-lock.json

RUN npm install --production

COPY . .

EXPOSE 3000

CMD [ "npm", "run", "apite" ]
