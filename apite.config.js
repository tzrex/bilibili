module.exports = {
	// 服务端口，命令行模式有效
	port: 3000,
	// 代理设置
	proxy: null,
	// api所在目录，相对根目录或绝对路径，默认为 api 目录
	dir: "src/router",
	// 静态文件目录，相对根目录或绝对路径,如： 'public'
	public: "public",
	// 文件兼听延时,为 0 时不兼听
	watchDelay: 300,
	// 是否格式化json输出
	jsonFormat: true,
	// 是否全局mock
	mock: false,
	// 请求地址前辍， 命令行模式默认为空，插件默认为 '/api'
	prefix: "",
	// 是否严格匹配请求类型
	strictMethod: false,
	// 返回数据格式
	defaultType: "",
	// 设置跨域，如: '*'
	crossDomain: "*",
	// jsonp请求callback请求参数
	jsonpCallback: "callback",
	// 文档生成路径，为空时不生成文档
	doc: "/",
	// 文档标题
	docTitle: "台沃智慧农业-经销商",
	// 文档描述，文本或markdown文档地址，相对于根目录
	docDesc: "经销商端mock的数据，支持crud",
	// 公共返回格式定义
	resp: {
		code: ["code", 0], // 成功字段，默认返回码
		fail: ["fail", 400], // 失败信息，默认返回码
		msg: ["message", "ok"], // 信息字段，默认值
		result: ["items"], // 结果字段
		total: ["total", 0] // 列表总数字段，默认值
	}
}
