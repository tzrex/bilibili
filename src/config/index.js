const mysql = require("mysql2/promise")

const TZ = process.env.TZ || "Asia/Shanghai"
const MysqlHost = process.env.MYSQL_HOST || "47.120.22.178"
const MysqlPort = process.env.MYSQL_PORT || "3306"
const MysqlDb = process.env.MYSQL_DATABASE || "mock"
const MysqlUser = process.env.MYSQL_USER || "mock"
const MysqlPass = process.env.MYSQL_PASSWORD || "xaA4v8r5LGnbLOXp"

const data = {
	TZ,
	MysqlHost,
	MysqlPort,
	MysqlDb,
	MysqlUser,
	MysqlPass
}

// 创建连接池，设置连接池的参数
const DB = mysql.createPool({
	host: MysqlHost,
	port: MysqlPort,
	password: MysqlPass,
	user: MysqlUser,
	database: MysqlDb,
	waitForConnections: true,
	connectionLimit: 20,
	maxIdle: 10,
	idleTimeout: 60 * 1000,
	queueLimit: 0,
	enableKeepAlive: true,
	supportBigNumbers: true,
	bigNumberStrings: true,
	keepAliveInitialDelay: 0
})

console.table({ data })

module.exports = {
	DB,
	data
}
