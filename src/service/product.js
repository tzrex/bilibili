const { DB } = require("../config")
const { listObjToTreeObj } = require("../utils")

/**
 *
 * @description 关联所有相关表进行联合查询
 * @param {*} isSingle 是否是查询单个
 * @param {*} options 查询时携带的参数配置
 */
async function getProducts(isSingle, options) {
	const { id, dealerId, pageSize = 5, pageNum = 1 } = options
	let args = []

	const baseSql =
		"SELECT " +
		" products.*," +
		// 分类信息
		" ptype.`id` AS ptypeId," +
		" ptype.`name` AS ptypeName," +
		" ptype.`createAt` AS ptypeCreateAt," +
		" ptype.`updatedAt` AS ptypeUpdatedAt, " +
		// 产品数量
		" dp.productNum, " +
		" CASE WHEN dp.productId IS NOT NULL " +
		" THEN 1 ELSE 0 " +
		" END AS isAccept " +
		" FROM " +
		"	`products` " +
		" INNER JOIN `product_class` AS ptype ON products.type = ptype.id " +
		" LEFT JOIN `dealer_products` AS dp ON products.id = dp.productId "

	let multiple = ""

	if (dealerId && dealerId !== "0") {
		multiple += " AND dp.dealerId = ? "
		args.push(dealerId)
	}

	try {
		if (isSingle) {
			multiple += " WHERE products.id=?"
			args.push(id)
		} else {
			const offset = (pageNum - 1) * pageSize
			multiple += ` LIMIT ${pageSize} OFFSET ${offset} `
			args.push(pageSize, offset)
		}

		const sql = baseSql + multiple

		const [rows] = await DB.execute(sql, args)

		const newList = rows.map((row) => {
			const item = listObjToTreeObj(row, [{ prefix: "ptype", id: "ptypeId" }])
			const type = item.ptype[0]
			item.type = type
			item.typeName = type.name

			delete item.ptype
			return item
		})

		return newList
	} catch (error) {
		console.error("error", error)
		throw error
	}
}

/**
 *
 * @description 简单的单表查询，不连表查询
 * @param {*} isSingle 是否是查询单个
 * @param {*} options 查询时携带的参数配置
 */
async function getSimpleProducts(options) {
	const baseSql = "SELECT * FROM `customers` LIMIT ? OFFSET ? "

	const { pageSize = 5, pageNum = 1 } = options

	try {
		const offset = (pageNum - 1) * pageSize

		const [rows] = await DB.execute(baseSql, [pageSize, offset])

		return rows
	} catch (error) {
		console.error("error", error)
		throw error
	}
}

/**
 * @description 经销商代理的产品
 * @param {*} dealerId 经销商唯一值
 */
async function acceptProducts(dealerId) {
	if (dealerId && dealerId !== "0") {
		const sql = "SELECT * FROM `dealer_products` AS dp WHERE dp.dealerId = ?"
		const args = [dealerId]

		try {
			const [rows] = await DB.execute(sql, args)
			return rows.map((item) => item.productId)
		} catch (error) {
			throw "查询失败"
		}
	}
}

/**
 *
 * @param {*} dealerId 经销商Id
 * @param {*} productId 产品ID
 * @param {*} number 新增加的数量
 */
async function productAddNumber(dealerId, productId, number) {
	const sql =
		"UPDATE `dealer_products` AS dp " +
		" SET dp.productNum = dp.productNum + ?" +
		" WHERE dp.dealerId = ? AND dp.productId = ? "

	const args = [number, dealerId, productId]

	try {
		const [rows] = await DB.execute(sql, args)

		return rows
	} catch (error) {
		throw "修改失败"
	}
}

/**
 *
 * @param {*} dealerId 经销商Id
 * @param {*} productId 产品ID
 * @param {*} number 新增加的数量
 */
async function addAccept(dealerId, productId, number) {
	const sql =
		"INSERT INTO `dealer_products` ( dealerId, productId, productNum )" +
		" VALUES  (?, ?, ?)"

	try {
		const [rows] = await DB.execute(sql, [dealerId, productId, number])

		return rows
	} catch (error) {
		throw "已有对应关系"
	}
}

module.exports = {
	getProducts,
	getSimpleProducts,
	acceptProducts,
	productAddNumber,
	addAccept
}
