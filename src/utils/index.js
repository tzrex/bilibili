/**
 *
 * @param {*} obj 需要转换的对象，由于传递的是对象的指针，所以会直接修改原对象
 * @param {*} startWith 属性的前缀信息，包含：prefix（前缀的字符串）和id（相关联的外键）的对象数组
 */
function listObjToTreeObj(obj, startWith) {
	if (!(startWith instanceof Array)) {
		console.error("startWith must be array")
		return
	}

	const copyObj = { ...obj } // obj内都是基础类型，所以直接简单拷贝就行

	// 遍历需要操作法属性
	for (let i = 0; i < startWith.length; i++) {
		const sw = startWith[i]

		if (!obj[sw.id]) return copyObj

		const frequency = obj[sw.id].split(",").length // 确定子数组属性的长度
		const list = []

		for (let i = 0; i < frequency; i++) {
			list.push({})
		}

		const keyArr = Object.keys(copyObj).filter((key) =>
			key.startsWith(sw.prefix)
		)

		for (let i = 0; i < keyArr.length; i++) {
			const key = keyArr[i]
			const attr = key.slice(sw.prefix.length)
			const newKey = attr[0].toLowerCase() + attr.slice(1)

			// 统一当作字符串处理
			copyObj[key] = "" + copyObj[key]
			copyObj[key] = copyObj[key].split(",")

			for (let i = 0; i < copyObj[key].length; i++) {
				list[i][newKey] = copyObj[key][i]
			}

			delete copyObj[key]
		}

		// 完成去重操作
		const newList = list
			.map((item) => JSON.stringify(item))
			.filter((item, index, self) => {
				return self.indexOf(item) === index
			})
			.map((item) => JSON.parse(item))

		copyObj[sw.prefix] = newList
	}

	return copyObj
}

module.exports = {
	listObjToTreeObj
}
