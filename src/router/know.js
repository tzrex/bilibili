/**
 * @sort 4
 * @name 知识板块
 */
const { api, delay, mock, resp } = require("apite")
const { expertImg } = require("../localData")

/**
 * @name 专家列表
 * @description 纯粹由mock创建的数据，数据之间不不关联
 */
api.get("/api/know/expert", async (ctx) => {
	await delay(70, 100)

	const data = mock({
		"list|20": [
			{
				id: "@guid",
				expertName: "@cname()",
				contact: /^1[385][1-9]\d{8}/,
				"expertise|1": [
					"种植技术",
					"养殖管理",
					"农业政策",
					"市场营销",
					"农业机械"
				],
				"eduBack|1": ["本科", "硕士", "博士", "博士后", "教授"],
				"affiliation|1": [
					"农业大学",
					"农业科研院",
					"农业公司",
					"农业协会",
					"农业部门"
				],
				videoCount: "@int(3,20)",
				"awards|1": [
					"省级优秀专家",
					"国家级科技奖",
					"国际农业奖",
					"行业杰出人士",
					"农业领军人物"
				],
				bio: "@title"
			}
		]
	})

	ctx.body = resp.list(data.list, 20)
})

/**
 * @name 专家讲堂
 * @description 纯粹由mock创建的数据，数据之间不不关联
 */
api.get("/api/know/eduroom", async (ctx) => {
	await delay(70, 100)

	const { total = 20 } = ctx.query

	const list = []

	for (let i = 0; i < total; i++) {
		const index = mock("@int(0,2)")
		const data = mock({
			id: "@guid",
			image: expertImg[index],
			text: "@csentence()",
			person: "@cname()",
			createdAt: '@datetime("yyyy-MM-dd")'
		})

		list.push(data)
	}

	ctx.body = resp.list(list, total)
})

/**
 * @name 农业知识
 */
api.get("/api/know/farming", async (ctx) => {
	await delay(70, 100)

	const { total = 20 } = ctx.query

	const list = []

	for (let i = 0; i < total; i++) {
		const cont = mock("@csentence(5)")

		const data = mock({
			id: "@guid",
			title: "@csentence()",
			cnt: "@int(300, 100000)",
			image: `@image(200x200, #02adea, ${cont})`,
			count: "@int(100, 4000)",
			author: "@cname()",
			createdAt: '@datetime("yyyy-MM-dd")'
		})

		list.push(data)
	}

	ctx.body = resp.list(list, total)
})

/**
 * @name 农业知识详情
 * @description 纯粹由mock创建的数据，数据之间不不关联
 */
api.get("/api/know/agricDetal", async (ctx) => {
	await delay(70, 100)

	const { id } = ctx.query
	if (!id) return (ctx.body = resp.fail("请传递id属性"))

	const cont = mock("@csentence(5)")

	const data = mock({
		id: "@guid",
		title: "@csentence()",
		cnt: "@int(300, 100000)",
		image: `@image(200x200, #02adea, ${cont})`,
		count: "@int(100, 4000)",
		author: "@cname()",
		createdAt: '@datetime("yyyy-MM-dd")',
		detail: "@cparagraph(5,7)"
	})

	ctx.body = resp.ok(data)
})
