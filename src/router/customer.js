/**
 * @sort 2
 * @name 客户数据
 * 接口描述信息，支持html标签
 */
const { api, resp } = require("apite")
const { getCustomers } = require("../service/customer")
const { getLength } = require("../service/common")

/**
 * @name 客户列表
 * @param {number} pageSize 每页大小
 * @param {number} pageNum 第几页，默认从一开始
 */
api.get("/api/customer/list", async (ctx) => {
	const { pageNum, pageSize } = ctx.query

	try {
		const numb = pageNum > 0 ? Number(pageNum) : 1
		const size = pageSize > 0 ? Number(pageSize) : 5

		const data = await getCustomers(false, {
			pageNum: numb,
			pageSize: size
		})

		const total = await getLength("customers", [])

		ctx.body = resp.list(data, total, { pageNum: numb, pageSize: size })
	} catch (error) {
		ctx.body = resp.fail("error")
	}
})

/**
 * @name 客户详情
 * @param {number,string} id 数据的唯一值
 */
api.get("/api/customer/detail", async (ctx) => {
	const { id } = ctx.query
	if (!id) {
		ctx.body = resp.fail("请传递ID属性")
		return
	}
	const numbId = Number(id)
	try {
		const data = await getCustomers(true, { id: numbId })

		if (data[0]) {
			ctx.body = resp.ok(data[0])
		} else {
			ctx.body = resp.fail("ID不存在")
		}
	} catch (error) {
		ctx.body = resp.fail(error)
	}
})
