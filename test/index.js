// const { listObjToTreeObj } = require("../src/utils")

// const obj = {
// 	id: 1,
// 	name: "张三",
// 	avatar: "/avatars/one.png",
// 	age: 35,
// 	gender: 1,
// 	phone: "13800138000",
// 	createAt: "2024-05-16T14:12:04.000Z",
// 	updatedAt: "2024-05-16T20:19:35.000Z",

// 	adsIds: "1,2",
// 	adsAddress: "北京市朝阳区,上海市浦东新区",
// 	adsLocal: "1号楼2单元,3号楼4单元",
// 	adsPhone: "13800138000,13800138000",
// 	adsManagerId: "1,1",
// 	adsCreateAt: "2024-05-16 22:12:04,2024-05-16 22:12:04",
// 	adsUpdatedAt: "2024-05-16 22:12:04,2024-05-16 22:12:04",

// 	tagIds: "1,2,1,2",
// 	tagNames: "金牌客户,老客户,金牌客户,老客户",
// 	tagTypes: "customer,customer,customer,customer",
// 	tagCreateAts:
// 		"2024-05-16 22:12:04,2024-05-16 22:12:04,2024-05-16 22:12:04,2024-05-16 22:12:04",
// 	tagUpdateAts:
// 		"2024-05-16 22:12:51,2024-05-16 22:12:57,2024-05-16 22:12:51,2024-05-16 22:12:57"
// }

// const resultObj = listObjToTreeObj(obj, [
// 	{ prefix: "ads", foreign: "1", id: "adsManagerId" },
// 	{ prefix: "tag", foreign: "customer", id: "tagTypes" }
// ])

// console.log("resultObj", resultObj)

const { getOrdersIds } = require("../src/service/orders")
getOrdersIds(4).then((list) => {
	console.log("list", list)
})
